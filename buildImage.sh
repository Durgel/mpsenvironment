#!/bin/sh
docker build \
    --build-arg UNAME=$USER     \
    --build-arg UID=$(id -u)    \
    --build-arg GID=$(id -u)    \
    -t mpsenv:debug .
