ARG UNAME="mpsuser"
ARG UID=1000
ARG GID=1000

FROM archlinux:base-devel AS build-base
    ARG UNAME
    ARG UID
    ARG GID
    RUN pacman -Sy --noconfirm archlinux-keyring
    RUN pacman -Syu --noconfirm
    RUN pacman -S --noconfirm base-devel git help2man python unzip wget audit rsync
    RUN groupadd -g $GID $UNAME
    RUN useradd -m --gid $GID --uid $UID $UNAME
    RUN echo "$UNAME    ALL=(ALL:ALL) ALL" >> /etc/sudoers

    USER $UNAME
    WORKDIR /home/$UNAME
    RUN mkdir crosstool && mkdir src

    WORKDIR crosstool
    RUN git clone https://github.com/crosstool-ng/crosstool-ng

    WORKDIR crosstool-ng
    RUN ./bootstrap
    RUN ./configure --prefix=/home/$UNAME/crosstool/crosstool-ng/.local
    RUN make -j$(nproc)
    RUN make install
    ENV PATH=/home/$UNAME/crosstool/crosstool-ng/.local/bin:$PATH

    WORKDIR ../
    COPY ./.config .
    RUN ct-ng -j$(nproc) build

    WORKDIR ..
    RUN tar -C ./x-tools -czf arm-eabi-toolchain.tar.gz ./arm-eabi/

FROM alpine:latest AS TestSystem
    ARG UNAME
    COPY --from=build-base /home/$UNAME/arm-eabi-toolchain.tar.gz /opt/
    WORKDIR /opt
    RUN tar -xf arm-eabi-toolchain.tar.gz
    ENV PATH=$PATH:/opt/arm-eabi-toolchain/bin
